#! /usr/bin/env bash
#
# Connect to MySQL using credentials from a configuration file
#
# (NOTE: Configuration file MUST use `.rc`-like syntax since it gets `source`d.)
#
# Usage
# - DEFAULTS:    `./mysql.sh`
# - CUSTOM PATH: `./mysql.sh /srv/http/app_name`
# - CUSTOM FILE: `./mysql.sh '' config.rc`
# - CUSTOM BOTH: `./mysql.sh /srv/http/app_name config.rc`
#
# Shellcheck directive reference
# SC-1090: Cannot find file being sourced
#   File may not exist until repository is cloned

# Set path of configuration file
CONFIG_PATH=${1:-/srv/http/foo} # arg1 or fallback if unset/null

# Set name of configuration file
CONFIG_FILE=${2:-.env} # arg2 or fallback if unset/null

# Import configuration + Connect
# shellcheck disable=1090
(source "$CONFIG_PATH/$CONFIG_FILE" && mysql -h "$DB_HOST" -u "$DB_USERNAME" -p"$DB_PASSWORD" "$DB_DATABASE")
