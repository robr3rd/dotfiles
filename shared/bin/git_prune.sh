#! /usr/bin/env bash
for branch in $(git branch -vv | grep gone | awk '{print $1}' | grep -v gone); do
	git branch -D "$branch"
done
