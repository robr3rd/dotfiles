#! /usr/bin/env zsh

# Debug Zsh startup time
# zmodload zsh/zprof

# Set '$ZSH' for OhMyZsh
export ZSH="$HOME/.oh-my-zsh"

# Directories to be prepended to $PATH
# Tip: To find the base path, use 'brew --prefix [package]'
# Tip Tip: ...but don't do it in '.zshrc' because each one could take ~1 second.
declare -a path_dirs
path_dirs=(
	"/usr/local/sbin"
	"/usr/local/bin"
	"/usr/bin"
	"/bin"
	"/usr/local/sbin"
	"/usr/sbin"
	"/sbin"
	"/usr/local/opt/coreutils/libexec/gnubin" # macOS: Add GNU core utils
	"/usr/lib/jvm/default/bin"
	"$HOME/.composer/vendor/bin"
	"$HOME/bin"
)

# Explicitly configured $PATH
PATH=""

for dir in ${(k)path_dirs[@]}
do
  if [ -d ${dir} ]; then
    # If this directory exists, then prepe it to the existing $PATH
    PATH="$PATH:${dir}"
  fi
done

unset path_dirs

export PATH



# Source
#source <(npm completion) # uncomment if desired

# Set name of the theme to load (hint: look in '~/.oh-my-zsh/themes/')
ZSH_THEME="agnoster"

# Right-hand side prompt
RPROMPT="%*" # prints time of input prompt creation (AKA end time of previous command)

# Auto-Updates
export UPDATE_ZSH_DAYS=13 # Update frequency

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
# NOTE: On macOS, 'bgnotify' requires `brew install terminal-notifier`.
plugins=(bgnotify colored-man-pages colorize composer docker docker-alias git gulp
	history history-substring-search nmap tmux vagrant yarn)



# With everything now setup, let's activate Oh-My-Zsh!
source "$ZSH/oh-my-zsh.sh"



# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
	export EDITOR='nano'
else
	export EDITOR='vim'
fi



# Functions
## Simple one-liners
### Make a directory and immediately move into it
function mkcd { mkdir -p $1; cd $1; }

### Display the output of 'ls -A' in list form rather than...
# ...the default and sometimes difficult to read "grid" format
function lsvertical { ls -lA $1 | awk '{print $9}'; }
alias lsv='lsvertical'

### Grep the Process List
function psg { ps waux | grep $1 | grep -v grep | grep -v "$0"; }



# Aliases
# Set personal aliases, overriding those provided by oh-my-zsh libs, plugins, and themes.
# Aliases can be placed here, though oh-my-zsh users are...
# ...encouraged to define aliases within the ZSH_CUSTOM folder.
#
# For a full list of active aliases, run `alias`.

[ -f ~/.zsh_aliases ] && source ~/.zsh_aliases

### Commented out due to macOS being incapable of processing mixed flag types (-- and -)
# alias rm='rm -i --preserve-root' # Prompt + save root
# alias chgrp='chgrp --preserve-root'
# alias chmod='chmod --preserve-root'
# alias chown='chown --preserve-root'



# Online help
autoload -U run-help
autoload run-help-git
autoload run-help-svn
autoload run-help-svk
export HELPDIR=~/zsh_help



# Fix for using Home/End Keys in PhpStorm's emulated terminal
# (source: https://github.com/robbyrussell/oh-my-zsh/issues/4784)
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line



# Some...magic?
function _is_command {
	which "$1" 2>&1 > /dev/null
	return $?
}
function _alias_if_not_exists {
	! _is_command "$1" && alias "$1"="$2"
}
_alias_if_not_exists help run-help



# Source local extra (private) settings specific to machine if it exists
[ -f ~/.zsh.local ] && source ~/.zsh.local



# Load `rbenv` automatically
#eval "$(rbenv init -)"



# Fish shell-like syntax highlighting (note: must be last)
## On macOS: 'brew install zsh-syntax-highlighting'
if [[ "$(uname -s)" == 'Darwin' ]]; then
	zsh_syn_hl_path="$(brew --prefix)/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

	if [ -f "$zsh_syn_hl_path" ]; then
		source "$zsh_syn_hl_path"
	fi
## On Linux: Follow upstream instructions, i.e., git glone repo --> cp *.zsh+highlighters+.revision-hash+.version==>/usr/share/zsh/plugins/zsh-syntax-highlighting/
elif [[ "$(uname -s)" == 'Linux' ]]; then
	zsh_syn_hl_path="/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

	if [ -f "$zsh_syn_hl_path" ]; then
		source "$zsh_syn_hl_path"
	fi
fi
