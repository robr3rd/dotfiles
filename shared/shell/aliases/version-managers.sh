#! /usr/bin/env bash
#
# Version Manager aliases
#
alias loadnvm='[ -d /usr/share/nvm ] && source /usr/share/nvm/init-nvm.sh'
alias loadrbenv='eval "$(rbenv init -)"'
