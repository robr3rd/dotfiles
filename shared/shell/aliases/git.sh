#! /usr/bin/env bash
#
# Git aliases
#

# General
alias gpwb='git symbolic-ref --short HEAD' # pwb = " print working branch"



# Branch
alias gb='git branch'
alias gbd='gb -d' # [branch] (delete)
alias gbD='gb -D' # [branch] (force delete)
alias gbda='gb --no-color --merged | \grep -vE "^(\*|\s*(master|develop|dev)\s*$)" | \xargs -n 1 git branch -d'

# Checkout
alias gco='git checkout' # [committish]
alias gcob='gco -b' # [new_branch_name]
alias gcom='gco master'
alias gundo='gco --' # [filepath] - "Undo" changes in working branc

# Commit
alias ga='git add'
alias gaa='ga . -A'
alias gc='git commit'
alias gca='gc --all --verbose'
alias gcam='gc --all -m' # 'gcam "[commit message]"'
alias 'gca!'='gc --amend --verbose'
alias 'gcan!'='gc --amend --no-edit' # Preserve commit message

# Diff
alias gd='git diff --name-status'
alias gdm='gd master'

# Fetch
alias gfa='git fetch --all'
alias gfap='gfa --prune'

# Log
alias gl='git log --pretty=format:"%C(auto)%h%C(reset) %s%C(auto)%d"'
alias gla='git log --pretty=format:"%C(auto)%h %C(blue)%ar%C(reset) %s %C(yellow)by %C(green)%an %C(red)<%aE>%C(auto)%d"' # gla = 'git log all'; use "all" (most) of the options
alias glg='gl --graph'
alias glag='gla --graph'
alias glo='git log --pretty=oneline'
alias glnm='git log --no-merges'
alias glast='git log -1 --pretty=%B | cat' # Last commit

# Merge
alias gm='git merge'
alias gmm='gm master'
alias gr='git rebase'
alias grm='gr master'

# Pull
alias gpl='git pull'
alias gpilr='gpl --rebase'
alias gplwb='gpl origin $(gpwb)' # gplwb = "git pull working branch"

# Push
alias gps='git push origin $(gpwb)'
alias gpsf='gps --force'

# Remote tracking
alias gpstrack='gpsnew'
alias gpstrackf='gpstrack --force'

# Reset
alias grs='git reset' # [filepath / SHA /branch]
alias grsh='grs --hard' # [filepath / SHA / branch]
alias grhom='grsh origin/master'

# Stash
alias gs='git stash'
alias gsl='gs list'
alias gsp='gs pop'
alias gsc='gs clear'

# Status
alias gst='git status'



# Combinations
## Fetching/Pulling/Cleaning
alias gfp='gfap && gplwb' # Ensure absolute up-to-dated-ness
#alias gfpr='gfp && gprune' # Fetch+Pull; Prune
#alias gfprda='gfp && gprune && gbda' # Fetch+Pull; Prune; Delete merged branches

alias gacp='gaa && gca && gps' # Add all, commit, push
alias gacpf='gaa && gca && gpsf' # Add all, commit, push (force)
alias 'gacpf!'='gaa && gca! && gpsf' # Add all, commit (amend), push (force)
alias 'gacpfn!'='gaa && gcan! && gpsf' # Add all, commit (amend, no-edit), push (force)
