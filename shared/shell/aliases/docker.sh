#! /usr/bin/env bash
#
# Docker aliases
#

# Plain ol' aliases
# Docker
alias dk='docker'

## Run
alias dkr='docker run --rm'


## Exec
alias dke='docker exec'

# Compose
alias dkcmp='docker-compose'
alias dkcmpb='dkcmp build'
alias dkcmpu='dkcmp up'
alias dkkill='docker stop $(docker ps -a -q)'

# Machine
alias dkm='docker-machine'
alias dkms='dkm ssh'
alias dkmu='eval $(dkm env --unset)'

# Container
alias dkc='dk container'
alias dkcl='dkc ls'
alias dkcla='dkcl -a'
alias dkcp='dkc prune'

# Images
alias dki='dk image'
alias dkil='dk image ls'
alias dkila='dkil -a'
alias dkip='dki prune'

# Networks
alias dkn='dk network'
alias dknl='dkn ls'
alias dknp='dkn prune'

# Volumes
alias dkv='dk volume'
alias dkvl='dkv ls'
alias dkvp='dkv prune'


# Complex helpers
alias dkprune='docker container prune && docker image prune && docker volume prune && docker network prune'
alias dkrun='docker run --rm --volume="$PWD:/my_dock" --workdir="/my_dock" --memory=4g --memory-swap=4g'


# Functions for more "advanced" customization
# Docker Machine 'activate' (like Python virtualenvs)
function dkma { eval "$(docker-machine env "$1")"; }

# Monitor a Docker Machine with Dockly
# note: everything in a subshell (params) so that the docker-machine context doesn't change
function dkmly { (eval "$(docker-machine env "$1")" &&  docker run --rm -it --volume /var/run/docker.sock:/var/run/docker.sock lirantal/dockly); }

# ShellCheck
function dkshellcheck {
	# shellcheck disable=2034 # This is (clearly) not something that can be verified via static analysis
	file="$(basename "${@:-1}")"
	directory="$(cd "$(dirname "${@:-1}")" && pwd)"

	# ${@:1:${#}-1} = "all but the last argument"
	all_but_last_arg=( "${@:1:$(($#-1))}" )
	(cd "$directory" && docker run --rm --volume "$(pwd)":/host koalaman/shellcheck-alpine:stable sh -c "cd /host && shellcheck ${all_but_last_arg[*]} $file");
	return "$?";
}



# ========================================
# Opinionated/"Shortcut" aliases/functions
# ----------------------------------------

# 'man' anything from within Debian
function dkman() { docker run --rm -it debian bash -c "apt-get update && apt-get -y install man && man ${1:man}"; }
