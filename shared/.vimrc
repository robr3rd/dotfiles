" vim-airline
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline_whitespace=1
let g:airline_theme = 'luna'
let g:airline_powerline_fonts = 1
let g:Powerline_symbols = 'fancy'


" gvim
if has("gui_running")
	let s:uname = system("uname")
	if s:uname == "Darwin\n" " (or MacVim)
		set guifont=Inconsolata-g\ for\ Powerline:h15
	endif
endif


" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_ansible_checkers = ['ansible_lint']
let g:syntastic_cucumber_checkers = ['cucumber']
let g:syntastic_dockerfile_checkers = ['dockerfile-lint']
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_php_checkers = ['php', 'phpcs', 'phpmd', 'phplint', 'phpstan']
let g:syntastic_scss_checkers = ['stylelint']
let g:syntastic_yaml_checkers = ['yamllint']
let g:syntastic_aggregate_errors = 1


" Clipboard
set clipboard=unnamed " use system clipboard as default


" UI stuff
syntax on
colorscheme delek
set number " show line numbers
set mouse=a " enable 'mouse mode'
set guifont=Hack:h15

" Line length ruler
" mark columns w/ ruler + make it a ["dark gray" color](http://vim.wikia.com/wiki/Xterm256_color_names_for_console_Vim)
set colorcolumn=80,120
highlight ColorColumn ctermbg=236

" Indentation Settions (a.k.a. 'Tabs vs. Spaces') - tips from here: http://vim.wikia.com/wiki/Indent_with_tabs,_align_with_spaces#Smart_tabs
set tabstop=4 " set 'tab indentation size' to 4
set shiftwidth=4
set softtabstop=0
set preserveindent
set copyindent
set noexpandtab
"set list listchars=tab:»·,trail:·,nbsp:· " Display extra whitespace
set encoding=utf-8
set t_Co=256
set term=xterm-256color
set termencoding=utf-8
set fillchars+=stl:\ ,stlnc:\

set incsearch " Show matches while searching
set hlsearch "Search highlighting

" Searching (or replacing) in uppercase will only match uppercase; doing the same in lowercase will match BOTH cases
set ignorecase
set smartcase

" Status line
set laststatus=2 " Show 'vim-airline' immediately rather than only on splits
set noshowmode " Hide the '-- INSERT --' mode indicator since 'vim-airline' already displays that information
set ttimeoutlen=10

set backspace=2

" For EditorConfig (Vundle) Plugin
" This allows the aforementioned plugin to enforce 'inset_final_newline'
set fixendofline

" Column ruler
"set textwidth=120
"set colorcolumn=+1 " Adds a marker at $textwidth (+1 so that it doesn't overlap the last character)

" Open new split panes to right and bottom, which is how most editors seem to do it
set splitbelow
set splitright

" Override swapfile storage location
" NOTE: (scope: only relevant for multi-user systems) This will prevent Vim from noticing when there are multiple users
" trying to edit the same file. Since the swap file isn't in the same
" directory as the original file -- instead being inside the active user's
" $HOME -- the other user's Vim won't know about the
" swap file and therefore won't know to warn that it's already being edited.
"
" Explanation of the syntax of this directive:
" Prepend (`^=`) a custom, preferred directory to the list of directories that Vim will try to use for swapfile storage.
" The trailing double-slash (`//`) tells Vim to use the absolute path to the file to create the swap file so that
" ...there aren't any collisions between files of the same name from different directories.
set directory^=$HOME/.vim/swapfiles//


" Enable edit history persistence
set undodir=~/.vim/undodir
set undofile


" Language-specific settings
" HTML
" Treat `<li>` and `<p>` as block tags -- which they are -- for indentation
let g:html_indent_tags = 'li\|p'
