#! /usr/bin/env bash

# General system commands
alias lsm='ls -lA | more'
alias ..='cd ..'
alias h='history'
alias dif='diff -u' # [original_file] [new_file] - Modern, unified output format
alias grepf='grep -l' # [remaining `grep` command] - No content, only filepath

# Import additional aliases
[ -f ~/.shell/aliases/docker.sh ] && source ~/.shell/aliases/docker.sh
[ -f ~/.shell/aliases/git.sh ] && source ~/.shell/aliases/git.sh
[ -f ~/.shell/aliases/vagrant.sh ] && source ~/.shell/aliases/vagrant.sh
[ -f ~/.shell/aliases/version-managers.sh ] && source ~/.shell/aliases/version-managers.sh
