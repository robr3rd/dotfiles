#! /usr/bin/env bash
#
# Symlink the files to the appropriate locations in the home directory (backup existing files, if applicable)
#


# Set project root
# Note: `export` is to make it available to any child scripts
if [ "$ROOT_PATH" == '' ]; then
	export ROOT_PATH
	ROOT_PATH="$(git rev-parse --show-toplevel)"
fi



# BACKUP
timestamp="$(date +%F-%T)"

# Ensure existence of necessary directories for backups
mkdir -p "$HOME/.robr3rd-dotfiles-${timestamp}.bak/.shell/aliases"

# Move if exists, otherwise just silently fail
mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.shell/aliases/docker.sh 2>/dev/null
mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.shell/aliases/git.sh 2>/dev/null
mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.shell/aliases/vagrant.sh 2>/dev/null
mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.shell/aliases/version-managers.sh 2>/dev/null

mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.bashrc 2>/dev/null
mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.bash_aliases 2>/dev/null
mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.git-completion.bash 2>/dev/null
mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.completion-wrapper.sh 2>/dev/null

mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.zshrc 2>/dev/null
mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.zsh_aliases 2>/dev/null

mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.tmux.conf 2>/dev/null
mv "$HOME"/{,.robr3rd-dotfiles-"$(date +%F-%T)".bak/}.vimrc 2>/dev/null



# INSTALL
# Ensure existence of necessary directories
mkdir -p $HOME/.shell/aliases

# Commence installation!
cp {"$ROOT_PATH/shared/shell","$HOME/.shell"}/aliases/docker.sh
cp {"$ROOT_PATH/shared/shell","$HOME/.shell"}/aliases/git.sh
cp {"$ROOT_PATH/shared/shell","$HOME/.shell"}/aliases/vagrant.sh
cp {"$ROOT_PATH/shared/shell","$HOME/.shell"}/aliases/version-managers.sh

cp {"$ROOT_PATH/shared/bash","$HOME"}/.bashrc
cp {"$ROOT_PATH/shared/bash","$HOME"}/.bash_aliases
cp {"$ROOT_PATH/shared/bash","$HOME"}/.git-completion.bash
cp {"$ROOT_PATH/shared/bash","$HOME"}/.completion-wrapper.sh

cp {"$ROOT_PATH/shared/zsh","$HOME"}/.zshrc
cp {"$ROOT_PATH/shared/zsh","$HOME"}/.zsh_aliases

cp {"$ROOT_PATH/shared","$HOME"}/.tmux.conf
cp {"$ROOT_PATH/shared","$HOME"}/.vimrc
