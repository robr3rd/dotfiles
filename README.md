# Dotfiles
Helpful shell login dot-files, such as `.bash_profile` and `.bashrc`.


## Installation
Run the `INSTALL.sh` script inside of the folder that represents your Operating System, e.g., `./linux/INSTALL.sh`.



## Features
### Automatic Installation
Using the included installation script(s), you can rest easy knowing that both file placement _and backups_ are taken care of for you.

Just sit back and relax with all of your originals politely archived in ~/.robr3rd-dotfiles-YYYY-MM-DD.bak.


### Performance
A nice blend between functionality and performance, Zsh (for example) loads in the same time it takes to say, "This is fast".

```shell
zsh -i -c exit  0.28s user 0.05s system 101% cpu 0.333 total
```


### Cross-platform compatibility
Except Windows. Because honestly, who uses the Windows command line?

Support for:

- Linux (obvious and easy)
- macOS (addresses some of the BSD-isms of macOS's non-GNU default binaries and works with `Terminal.app`'s weird login shell policy)



# Contents
```bash
$ tree --noreport -Fa -L 4 -I .git
.
├── .editorconfig
├── LICENSE
├── README.md
├── clean-backups.sh*
├── linux/
│   ├── INSTALL.sh*
│   └── bash/
│       └── .bash_aliases
├── macos/
│   ├── INSTALL.sh*
│   └── bash/
│       ├── .bash_aliases
│       ├── .bash_profile
│       └── .bashrc
└── shared/
    ├── .htoprc
    ├── .tmux.conf
    ├── .vimrc
    ├── INSTALL.sh*
    ├── bash/
    │   ├── .bash_aliases
    │   ├── .bashrc
    │   ├── .completion-wrapper.sh
    │   └── .git-completion.bash*
    ├── bin/
    │   ├── git_prune.sh*
    │   ├── mysql.sh
    │   └── redis.sh
    ├── shell/
    │   └── aliases/
    │       ├── docker.sh
    │       ├── git.sh
    │       ├── vagrant.sh
    │       └── version-managers.sh
    └── zsh/
        ├── .zsh_aliases
        └── .zshrc
```



### Git autocompletion
The included file (`shared/.git-completion.bash`) should be fine, but for an up-to-date edition (if desired), run: `curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ./shared/.git-completion.bash`.



## Roadmap
- Convert list of files in `shared/INSTALL.sh` to an array.
- Allow other scripts (e.g., `{linux,macos}/INSTALL.sh`) to pass in files as arguments to `shared/INSTALL.sh` which simply iterates over them like `FILE_LIST+=($x)` to add them to the list of things to do.  Example use case being macOS's `.bash_profile`, which is currently being handled in `macos/INSTALL.sh` and is frequently forgotten when the backup folder naming convention changes, for instance.
- I could definitely integrate an automated check for `.git-completion.bash` being up-to-date in the future via `diff <(curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash) .git-completion.bash`)



# Contributing
If directories/files change, please be sure to update the 'Contents' section of the README; it provides the command with which to do so.
