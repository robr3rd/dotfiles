#! /usr/bin/env bash

# Set project root
# Note: `export` is to make it available to any child scripts
if [ "$ROOT_PATH" == '' ]; then
	export ROOT_PATH
	ROOT_PATH="$(git rev-parse --show-toplevel)"
fi


# Execute shared installation script
"$ROOT_PATH/shared/INSTALL.sh"


# Platform-specific differences
## macOS requires extra files for Bash
### `bash_profile`
#### Backup
mv "$HOME/{,.robr3rd-dotfiles-$(date +%F-%T).bak/}.bash_profile" 2>/dev/null
#### Install
cp {"$ROOT_PATH"/macos/bash,"$HOME"}/.bash_profile


## macOS has some specific aliases to concetenate on top of the standard ones
cat "$ROOT_PATH"/{shared,macos}/bash/.bashrc > "$HOME"/.bashrc
cat "$ROOT_PATH"/{shared,macos}/bash/.bash_aliases > "$HOME"/.bash_aliases
