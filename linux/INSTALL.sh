#! /usr/bin/env bash

# Set project root
# Note: `export` is to make it available to any child scripts
if [ "$ROOT_PATH" == '' ]; then
	export ROOT_PATH
	ROOT_PATH="$(git rev-parse --show-toplevel)"
fi


# Execute shared installation script
"$ROOT_PATH/shared/INSTALL.sh"


# Platform-specific differences
## Concatenate
cat "$ROOT_PATH"/{shared,linux}/bash/.bash_aliases > "$HOME/.bash_aliases"
